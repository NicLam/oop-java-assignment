CREATE TABLE IF NOT EXISTS Item(
	Item_ID		INTEGER UNIQUE,
	Name		VARCHAR(128) NOT NULL,
	Mass		DECIMAL NOT NULL,
	Size		VARCHAR(20) NOT NULL,
	Fragile		BOOLEAN NOT NULL,
	Broken		BOOLEAN DEFAULT 0,
	PRIMARY KEY(Item_ID)
);

CREATE TABLE IF NOT EXISTS Parcel_Class_Data(
	Parcel_Class	INTEGER NOT NULL,
	Max_Mass	DECIMAL NOT NULL,
	Max_Size	VARCHAR(20) NOT NULL,
	Fragile 	BOOLEAN,
	PRIMARY KEY(Parcel_Class)
);

CREATE TABLE IF NOT EXISTS Parcel(
	Parcel_ID	INTEGER NOT NULL UNIQUE,
	Parcel_Class	INTEGER,
	PRIMARY KEY(Parcel_ID),
	FOREIGN KEY(Parcel_Class) REFERENCES Parcel_Class_Data(Parcel_Class)
);

CREATE TABLE IF NOT EXISTS Parcel_Contains(
	PC_ID		INTEGER NOT NULL UNIQUE,
	Parcel_ID	INTEGER NOT NULL,
	Item_ID		INTEGER NOT NULL,
	PRIMARY KEY(PC_ID),
	FOREIGN KEY(Parcel_ID) REFERENCES Parcel(Parcel_ID),
	FOREIGN KEY(Item_ID) REFERENCES Item(Item_ID)
);

CREATE TABLE IF NOT EXISTS Log(
	Log_Entry_ID	INTEGER NOT NULL,
	Log_Date	DATE DEFAULT CURRENT_DATE,
	User_Actions	VARCHAR(512),
	PRIMARY KEY(Log_Entry_ID)
);

CREATE TABLE IF NOT EXISTS Delivery(
	Delivery_ID		INTEGER NOT NULL UNIQUE,
	Parcel_ID		INTEGER NOT NULL,
	Log_Entry_ID		INTEGER NOT NULL,
	Parcel_Destination 	INTEGER NOT NULL,
	Parcel_Origin		INTEGER NOT NULL,
	Delivery_Start_Time	TIMESTAMP DEFAULT CURRENT_TIME,
	Delivery_Date		DATE DEFAULT CURRENT_DATE,
	User_Actions		VARCHAR(512),
	PRIMARY KEY(Delivery_ID),
	FOREIGN KEY(Parcel_ID) REFERENCES Parcel(Parcel_ID),
	FOREIGN KEY(Log_Entry_ID) REFERENCES Log(Log_Entry_ID),
	FOREIGN KEY(Parcel_Destination) REFERENCES Smartpost(Smartpost_ID),
	FOREIGN KEY(Parcel_Origin) REFERENCES Smartpost(Smartpost_ID)
);

CREATE TABLE IF NOT EXISTS Postal_Code_City(
	Postal_Code	VARCHAR(5) NOT NULL UNIQUE,
	City		VARCHAR(35) NOT NULL,
	PRIMARY KEY(Postal_Code)
);

CREATE TABLE IF NOT EXISTS Smartpost(
	Smartpost_ID	INTEGER NOT NULL UNIQUE,
	Postal_Code	VARCHAR(5) NOT NULL,
	Address		VARCHAR(50) NOT NULL,
	Availability	VARCHAR(50) NOT NULL,
	Latitude	DECIMAL NOT NULL,
	Longitude	DECIMAL NOT NULL,
	PRIMARY KEY(Smartpost_ID),
	FOREIGN KEY(Postal_Code) REFERENCES Postal_Code_City(Postal_Code)
);

CREATE TABLE IF NOT EXISTS Receipt(
	Receipt_ID		INTEGER NOT NULL UNIQUE,
	Receipt_Date		DATE DEFAULT CURRENT_DATE,
	Shipped_Parcels 	INTEGER NOT NULL,
	Received_Parcels	INTEGER NOT NULL,
	In_warehouse		INTEGER NOT NULL,
	PRIMARY KEY(Receipt_ID)
);

CREATE VIEW IF NOT EXISTS SPData AS select 
	Smartpost.Postal_Code, 
	City, 
	Address, 
	Availability FROM Smartpost 
	INNER JOIN Postal_Code_City ON Smartpost.Postal_Code = Postal_Code_City.Postal_Code;

INSERT OR REPLACE INTO Parcel_Class_Data (Parcel_Class, Max_Mass, Max_Size, Fragile) VALUES (1, 12.0, '11x36x60', 1);
INSERT OR REPLACE INTO Parcel_Class_Data (Parcel_Class, Max_Mass, Max_Size, Fragile) VALUES (2, 24.0, '37x36x60', 0);
INSERT OR REPLACE INTO Parcel_Class_Data (Parcel_CLass, Max_Mass, Max_Size, Fragile) VALUES (3, 36.0, '59x36x60', 0);

INSERT OR REPLACE INTO Item (Item_ID, Name, Mass, Size, Fragile) VALUES (1, 'Uolevin Aloe vera voide 5kg', 5, '20*20*20', 0);
INSERT OR REPLACE INTO Item (Item_ID, Name, Mass, Size, Fragile) VALUES (2, 'Hässi -lehti', 1, '20*10*20', 0);
INSERT OR REPLACE INTO Item (Item_ID, Name, Mass, Size, Fragile) VALUES (3, 'Jaloviina 0.5', 1, '10*10*10', 1);
INSERT OR REPLACE INTO Item (Item_ID, Name, Mass, Size, Fragile) VALUES (4, 'Tux', 20, '59*36*60', 0);

/*
SELECT lauseet joita ohjelma suorittaa ajon aikana
Java huolehtii että tietotyyppi on oikea ja kysely oikeellinen

SELECT DISTINCT Address FROM SPData WHERE City = -String-;
SELECT * FROM Item LIMIT 1 OFFSET -int-;
SELECT * FROM Item;
SELECT DISTINCT City FROM SPData;
SELECT Parcel_Destination, Parcel_Origin, User_Actions FROM Delivery WHERE Delivery_Date = -date-;"
SELECT SUM(Shipped_parcels), SUM(Received_parcels), SUM(In_warehouse) FROM Receipt WHERE Receipt_Date = -date-;
SELECT DISTINCT * FROM SPData WHERE City = -String-;
SELECT DISTINCT City FROM Postal_Code_City;
SELECT latitude, longitude FROM Smartpost WHERE Smartpost_ID = -int- OR Smartpost_ID = -int-;
SELECT Count(Item_ID) FROM Item;
SELECT Count(Parcel_ID) FROM Parcel;
SELECT Count(Receipt_ID) FROM Receipt;
*/
