package application;

import com.sun.rowset.CachedRowSetImpl;

import javax.sql.rowset.CachedRowSet;
import java.util.ArrayList;
import java.sql.*;

/**
 * Created by niclas on 7/4/17.
 * This class is used for inserting data into SQL database. XMLParser class parses it, this one is merely for SQL functions & connections
 * so XMLParser class doesn't get bloated.
 *
 * Tables that will be initialized listed below.
 * Alternatively we could just run the sql file but this way program isn't dependent on it.
 * Tables and their details are listed in the documentation.
 *
 * This class utilizes CachedRowSet to return a ReturnSet like object.
 * Can't return ReturnSet objects since it'd require to keep connection and statement open, which doesn't work in the long run (-> Database is locked error)
 */

class DatabaseConnections {
    //Location/Name of the database. (Check working directory)
    private static String url = "jdbc:sqlite:harkkaDB.db";

    private static final String initializeItem = "CREATE TABLE IF NOT EXISTS Item (" +
        "Item_ID            INTEGER UNIQUE, " +
        "Name               VARCHAR(128) NOT NULL," +
        "Mass               DECIMAL NOT NULL," +
        "Size               VARCHAR(20) NOT NULL," +
        "Fragile            BOOLEAN NOT NULL," +
        "Broken             BOOLEAN DEFAULT 0," +
        "PRIMARY KEY(Item_ID));";

    private static final String initializeParcel_Class_Data = "CREATE TABLE IF NOT EXISTS Parcel_Class_Data (" +
        "Parcel_Class       INTEGER NOT NULL," +
        "Max_Mass           DECIMAL NOT NULL," +
        "Max_Size           VARCHAR(20) NOT NULL," +
        "Fragile            BOOLEAN," +
        "PRIMARY KEY(Parcel_Class));";

    private static final String initializeParcel = "CREATE TABLE IF NOT EXISTS Parcel (" +
        "Parcel_ID          INTEGER NOT NULL UNIQUE," +
        "Parcel_Class       INTEGER," +
        "PRIMARY KEY(Parcel_ID)," +
        "FOREIGN KEY(Parcel_Class) REFERENCES Parcel_Class_Data(Parcel_Class));";

    //it could keep track of all the parcels and all its items
    private static final String initializeParcel_Contains = "CREATE TABLE IF NOT EXISTS Parcel_Contains(" +
        "PC_ID              INTEGER NOT NULL UNIQUE," +
        "Parcel_ID          INTEGER NOT NULL," +
        "Item_ID            INTEGER NOT NULL," +
        "PRIMARY KEY(PC_ID)," +
        "FOREIGN KEY(Parcel_ID) REFERENCES Parcel(Parcel_ID), " +
        "FOREIGN KEY(Item_ID) REFERENCES Item(Item_ID));";

    private static final String initializeLog = "CREATE TABLE IF NOT EXISTS Log (" +
        "Log_Entry_ID       INTEGER NOT NULL," +
        "Log_Date           DATE DEFAULT CURRENT_DATE," +
        "User_Actions       VARCHAR(512)," +
        "PRIMARY KEY(Log_Entry_ID));";

    private static final String initializeDelivery = "CREATE TABLE IF NOT EXISTS Delivery(" +
        "Delivery_ID            INTEGER NOT NULL UNIQUE," +
        "Parcel_ID              INTEGER NOT NULL," +
        "Log_Entry_ID           INTEGER NOT NULL," +
        "Parcel_Destination     INTEGER NOT NULL," +
        "Parcel_Origin          INTEGER NOT NULL," +
        "Delivery_Start_Time    TIMESTAMP DEFAULT CURRENT_TIME," +
        "Delivery_Date          DATE DEFAULT CURRENT_DATE," +
        "User_Actions           VARCHAR(512)," +
        "PRIMARY KEY(Delivery_ID)," +
        "FOREIGN KEY(Parcel_ID) REFERENCES Parcel(Parcel_ID)," +
        "FOREIGN KEY(Log_Entry_ID) REFERENCES Log(Log_Entry_ID)," +
        "FOREIGN KEY(Parcel_Destination) REFERENCES Smartpost(Smartpost_ID)," +
        "FOREIGN KEY(Parcel_Origin) REFERENCES Smartpost(Smartpost_ID))";

    //A table that consists of unqiue postal code and gives the city linked to it
    private static final String initializePostal_Code_City = "CREATE TABLE IF NOT EXISTS Postal_Code_City(" +
        "Postal_Code        VARCHAR(5) NOT NULL UNIQUE," +
        "City               VARCHAR(35) NOT NULL," +
        "PRIMARY key(Postal_Code));";

    private static final String initializeSmartpost = "CREATE TABLE IF NOT EXISTS Smartpost ("+
        "Smartpost_ID       INTEGER NOT NULL UNIQUE," +
        "Postal_Code        VARCHAR(5) NOT NULL," +
        "Address            VARCHAR(50) NOT NULL," +
        "Availability       VARCHAR(50) NOT NULL," +
        "Latitude           DECIMAL NOT NULL," +
        "Longitude          DECIMAL NOT NULL," +
        "PRIMARY KEY(Smartpost_ID)," +
        "FOREIGN KEY(Postal_Code) REFERENCES Postal_Code_City(Postal_Code));";


    private static final String initializeReceipt = "CREATE TABLE IF NOT EXISTS Receipt (" +
        "Receipt_ID         INTEGER NOT NULL UNIQUE," +
        "Receipt_Date       DATE DEFAULT CURRENT_DATE," +
        "Shipped_parcels    INTEGER NOT NULL," +
        "Received_parcels   INTEGER NOT NULL," +
        "In_warehouse       INTEGER NOT NULL," +
        "PRIMARY KEY(Receipt_ID));";

    //Create a view for easier access to data.
    private static final String initializeSPView = "CREATE VIEW IF NOT EXISTS SPData AS select Smartpost.Postal_Code, City, Address, Availability from Smartpost INNER JOIN Postal_Code_City ON Smartpost.Postal_Code = Postal_Code_City.Postal_Code;";

    //Initialize 3 unique parcel classes, each has different properties.
    private static final String firstClassParcelData  = "INSERT OR REPLACE INTO Parcel_Class_Data (Parcel_Class, Max_Mass, Max_Size, Fragile) VALUES (1, 12.0, '11x36x60', 1);";
    private static final String secondClassParcelData = "INSERT OR REPLACE INTO Parcel_Class_Data (Parcel_Class, Max_Mass, Max_Size, Fragile) VALUES (2, 24.0, '37x36x60', 0);";
    private static final String thirdClassParcelData  = "INSERT OR REPLACE INTO Parcel_Class_Data (Parcel_CLass, Max_Mass, Max_Size, Fragile) VALUES (3, 36.0, '59x36x60', 0);";

    //Initialize 4 items from database. (Users can also add their own items in the GUI)
    private static final String firstItem   = "INSERT OR REPLACE INTO Item (Item_ID, Name, Mass, Size, Fragile) VALUES (1, 'Uolevin Aloe vera voide 5kg', 5, '20*20*20', 0);";
    private static final String secondItem  = "INSERT OR REPLACE INTO Item (Item_ID, Name, Mass, Size, Fragile) VALUES (2, 'Hässi -lehti', 1, '20*10*20', 0);";
    private static final String thirdItem   = "INSERT OR REPLACE INTO Item (Item_ID, Name, Mass, Size, Fragile) VALUES (3, 'Jaloviina 0.5', 1, '10*10*10', 1);";
    private static final String fourthItem  = "INSERT OR REPLACE INTO Item (Item_ID, Name, Mass, Size, Fragile) VALUES (4, 'Tux', 20, '59*36*60', 0);";

    static void initializeTables() {
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(url); //Prepare the connection to database listed above
            Statement statement = connection.createStatement();

            statement.execute(initializePostal_Code_City); //Execute each statement
            statement.execute(initializeSmartpost);
            statement.execute(initializeItem);
            statement.execute(initializeParcel_Class_Data);
            statement.execute(initializeParcel);
            statement.execute(initializeParcel_Contains);
            statement.execute(initializeLog);
            statement.execute(initializeDelivery);
            statement.execute(initializeSPView);
            statement.execute(initializeReceipt);

            statement.execute(firstClassParcelData);
            statement.execute(secondClassParcelData);
            statement.execute(thirdClassParcelData);

            statement.execute(firstItem);
            statement.execute(secondItem);
            statement.execute(thirdItem);
            statement.execute(fourthItem);

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (connection != null) {connection.close();}
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }


    //Add entries to Smartpost table
    static void insertSmartpost(int Smartpost_ID, String postal_code, String address, String availability, double latitude, double longitude) {
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(url); //Get SQL connection & use prepared statement to avoid sql injection
            PreparedStatement pstmnt = connection.prepareStatement("INSERT OR REPLACE INTO Smartpost (Smartpost_ID, Postal_Code, Address, Availability, Latitude, Longitude) VALUES (?, ?, ?, ?, ?, ?);");
            pstmnt.setInt(1, Smartpost_ID);
            pstmnt.setString(2, postal_code);
            pstmnt.setString(3, address);
            pstmnt.setString(4, availability);
            pstmnt.setDouble(5, latitude);
            pstmnt.setDouble(6, longitude);
            pstmnt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (connection != null) {connection.close();} //Close connection
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }


    //Add entries to Postal_Code_City table
    static void insertPostalCodeCity(String postal_code, String city) {
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(url);
            PreparedStatement pstmnt = connection.prepareStatement("INSERT OR REPLACE INTO Postal_Code_City (Postal_Code, City) VALUES (?, ?);");
            pstmnt.setString(1, postal_code);
            pstmnt.setString(2, city);
            pstmnt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (connection != null) {connection.close();}
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }


    //Add entries to Parcel_Contains table
    static void insertParcel_Contains(int Parcel_ID, ArrayList<Item> contents) {
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(url);
            for (Item item : contents) {
                PreparedStatement pstmnt = connection.prepareStatement("INSERT OR REPLACE INTO Parcel_Contains (Parcel_ID, Item_ID) VALUES (?, ?);");
                pstmnt.setInt(1, Parcel_ID);
                pstmnt.setInt(2, item.getItem_ID());
                pstmnt.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (connection != null) {connection.close();}
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    //Add entries to Parcel table
    static void insertParcel(int parcel_ID, int parcel_Class) {
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(url);
            PreparedStatement preparedStatement = connection.prepareStatement("INSERT OR REPLACE INTO Parcel (Parcel_ID, Parcel_Class) VALUES (?, ?);");
            preparedStatement.setInt(1, parcel_ID);
            preparedStatement.setInt(2, parcel_Class);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (connection != null) {connection.close();}
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }


    //To Log table
    static void insertLog(int id, String user_actions) {
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(url);
            PreparedStatement preparedStatement = connection.prepareStatement("INSERT OR REPLACE INTO Log (Log_Entry_ID, User_Actions) VALUES (?, ?);");
            preparedStatement.setInt(1, id);
            preparedStatement.setString(2, user_actions);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (connection != null) {connection.close();}
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }


    //Delivery...
    static void insertDelivery(int logID, int parcelID, int parcelOrigin, int parcelDestination, String userActions) {
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(url);
            PreparedStatement preparedStatement = connection.prepareStatement("INSERT OR REPLACE INTO Delivery (Log_Entry_ID, Parcel_ID, Parcel_Destination, Parcel_Origin, User_Actions) VALUES (?, ?, ?, ?, ?);");
            preparedStatement.setInt(1, logID);
            preparedStatement.setInt(2, parcelID);
            preparedStatement.setInt(3, parcelOrigin);
            preparedStatement.setInt(4, parcelDestination);
            preparedStatement.setString(5, userActions);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (connection != null) {connection.close();}
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }


    //Add entries to Item table
    static void insertItem(int id, String name, String size, Double mass, boolean fragile) {
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(url);
            PreparedStatement pstmnt = connection.prepareStatement("INSERT INTO Item (Item_ID, Name, Size, Mass, Fragile) VALUES (?, ?, ?, ?, ?);");
            pstmnt.setInt(1, id);
            pstmnt.setString(2, name);
            pstmnt.setString(3, size);
            pstmnt.setDouble(4, mass);
            pstmnt.setBoolean(5, fragile);
            pstmnt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (connection != null) {connection.close();}
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }


    //And the receipt.
    static void insertReceipt(int receiptID, int shippedParcels, int receivedParcels, int parcelsInWarehouse) {
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(url);
            PreparedStatement pstmnt = connection.prepareStatement("INSERT INTO Receipt (Receipt_ID, Shipped_parcels, Received_parcels, In_warehouse) VALUES (?, ?, ?, ?);");
            pstmnt.setInt(1, receiptID);
            pstmnt.setInt(2, shippedParcels);
            pstmnt.setInt(3, receivedParcels);
            pstmnt.setInt(4, parcelsInWarehouse);
            pstmnt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (connection != null) {connection.close();}
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }


    //Functions can call this should they need data from database. This will return the ResultSet -like object CachedRowSet crs for them to parse and work on.
    static ResultSet getResultset(String query) {
        Connection connection = null;
        ResultSet rset = null;
        CachedRowSet crs = null;
        try {
            connection = DriverManager.getConnection(url);
            Statement statement = connection.createStatement();
            rset = statement.executeQuery(query);

            crs = new CachedRowSetImpl();
            crs.populate(rset);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (rset != null) {rset.close();}
                if (connection != null) {connection.close();}
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return crs;
    }
}
