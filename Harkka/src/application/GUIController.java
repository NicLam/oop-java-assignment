package application;

import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXDatePicker;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;

import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;

public class GUIController {
    private Warehouse warehouse;
    private static final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy HH.mm.ss"); //For logging
    private int logID = 1; //Keep list of log ids

    @FXML
    private WebView webView;

    @FXML
    private JFXDatePicker dateSelection;

    @FXML
    private JFXComboBox addCities, selectParcel;

    @FXML
    private JFXTextField sqlCommand;

    @FXML
    private JFXTextArea logTextArea;

    //initialize() is ran when the instance is created
    public void initialize() {
        warehouse = Warehouse.getInstance();
        logTextArea.setEditable(false);
        webView.getEngine().load(getClass().getResource("index.html").toExternalForm()); //Load the index.html from package, it has to be in IDE working folder
        populateComboBox(); //comboBox of cities

    }


    public void openGraph() {
        GraphController graphController = null;
        if (dateSelection.getValue() != null) {
            LocalDate date = dateSelection.getValue();

            try {
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("Graph.fxml")); //Graph has its own fxml
                Parent root = fxmlLoader.load();
                graphController = fxmlLoader.getController(); //Instance of the just created graph window
                graphController.graphTextNoneditable();

                Stage stage = new Stage();
                stage.setTitle(date.toString());
                stage.setScene(new Scene(root)); //Create scene from the root (loaded fxml) and then Stage out of it.
                stage.show(); //Show the stage

            } catch (Exception e) {
                System.out.println("Virhe luotaessa diagrammia");
            }

            //Queries to database to get needed data for graphs.
            String query = "SELECT Parcel_Destination, Parcel_Origin, User_Actions FROM Delivery WHERE Delivery_Date = '" + date + "';";
            String warehouseQuery = "SELECT SUM(Shipped_parcels), SUM(Received_parcels), SUM(In_warehouse) FROM Receipt WHERE Receipt_Date = '" + date + "';";
            ResultSet rs  = DatabaseConnections.getResultset(query);
            ResultSet rs1 = DatabaseConnections.getResultset(warehouseQuery);
            int shippedParcels = 0, receivedParcels = 0, warehouseParcels = 0;

            try {
                while (rs.next()) {
                    int parcelOrigin      = rs.getInt("Parcel_Origin");
                    int parcelDestination = rs.getInt("Parcel_Destination");
                    String userActions    = rs.getString("User_Actions");
                    graphController.setGraphText("Paketti kohteesta " + parcelOrigin + " kohteeseen " + parcelDestination  + " Lisätietoja: " + userActions + "\n");
                }

                while (rs1.next()) {
                    shippedParcels   = rs1.getInt(1);
                    receivedParcels  = rs1.getInt(2);
                    warehouseParcels = rs1.getInt(3);
                }

                //Draw graphs with data
                graphController.drawShipped(shippedParcels);
                graphController.drawReceived(receivedParcels);
                graphController.drawParcelsInWarehouse(warehouseParcels);

                //Close connections
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                try {
                    if (rs != null) {rs.close();}
                    if (rs1 != null) {rs1.close();}
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }


    //Create log entries of delivery when parcel is shipped
    void deliveryEntry(Parcel parcel, String userActions) {
        DatabaseConnections.insertDelivery(logID, parcel.getParcelID(), parcel.getOrigin_ID(), parcel.getDestination_ID(), userActions);
        DatabaseConnections.insertLog(logID, userActions);
        logEntry(userActions);
    }


    //Create log entry for whatever user action was
    void logEntry(String userActions) {
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        userActions = simpleDateFormat.format(timestamp) + " " + userActions;
        logTextArea.appendText(userActions + "\n");
        DatabaseConnections.insertLog(logID, userActions);
        logID++;
    }


    //Open a new window for creating the parcels
    public void createParcel() {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("ParcelFXML.fxml")); //Open up a new page for parcel creation
            Parent root1 = fxmlLoader.load();
            Stage stage = new Stage();

            stage.setTitle("Create a new parcel");
            stage.setScene(new Scene(root1));
            stage.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void clearRoutes() {
        webView.getEngine().executeScript("document.deletePaths()");
    }


    public void dropDB() { //Delete database.
        try {
            File file = new File("harkkaDB.db");
            if (file.delete()) {
                System.out.println("Database deleted.");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    //Get data from the view we created earlier, this function as a whole adds specific city's automatons to map
    public void automatonsToMap() {
        if (addCities.getSelectionModel().getSelectedItem() != null) { //Make sure selected item isn't null
            String city = addCities.getSelectionModel().getSelectedItem().toString(); //Get selected item
            String query = "SELECT DISTINCT * FROM SPData WHERE City = '" + city + "';";
            ResultSet rs = DatabaseConnections.getResultset(query);
            try {
                while (rs.next()) {
                    String postalCode = rs.getString("Postal_Code");
                    String address = rs.getString("Address");
                    String availability = rs.getString("Availability");
                    String script = "document.goToLocation('" + address + " " + postalCode + " " + city + "', '" + availability + "', 'orange')";
                    webView.getEngine().executeScript(script);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                try {
                    if (rs != null) {rs.close();}
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }


    //Add cities to the box.
    private void populateComboBox() {
        ResultSet rs = DatabaseConnections.getResultset("SELECT DISTINCT City FROM Postal_Code_City;");
        try {
            while (rs.next()) {
                String city = rs.getString("City");
                addCities.getItems().addAll(city);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {rs.close();}
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }


    //Run any SQL command.
    public void runCommand() {
        if (sqlCommand.getText() != null) {
            ResultSet rset = DatabaseConnections.getResultset(sqlCommand.getText());
            System.out.println(rset);
            //TODO if there's enough time. I just want to print everything the query gets
        }
    }


    public void createDB() {
        DatabaseConnections.initializeTables();
    }


    public void sendParcel() {
        if (selectParcel.getSelectionModel().getSelectedItem() != null) { //Get selected parcel if not null
            Parcel parcel = (Parcel) selectParcel.getSelectionModel().getSelectedItem();
            selectParcel.getSelectionModel().clearSelection(); //clear selection and add and to warehouse
            Warehouse.shipParcel(parcel);
        }
    }

    //This just updates every parcel on the list when the combobox is clicked.
    public void selectParcelMethod() {
        selectParcel.getItems().clear();
        ArrayList<Parcel> allParcels = Warehouse.getListOfParcels();
        for (Parcel parcel : allParcels) {
            selectParcel.getItems().addAll(parcel);
        }
    }


    //Same as JavaScript function createPath in index.html but this just returns distance without creating the path. Used for making sure that distance doesn't exceed 150km
    double getDistance(int origin_ID, int destination_ID) {
        ArrayList<String> geolocations = new ArrayList<>();
        String query = "SELECT latitude, longitude FROM Smartpost WHERE Smartpost_ID = '" + origin_ID + "' OR Smartpost_ID = '" + destination_ID + "';";

        ResultSet rs = DatabaseConnections.getResultset(query);

        //Do a query to SQL to get Smartpost IDs, then send them to the JavaScript API to get distance between them
        try {
            while (rs.next()) {
                String latitude = String.valueOf(rs.getDouble("latitude"));
                String longitude = String.valueOf(rs.getDouble("longitude"));
                geolocations.add(latitude);
                geolocations.add(longitude);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {rs.close();}
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        String script = "document.getDistance(" + geolocations + ")";
        return Double.valueOf(String.valueOf(webView.getEngine().executeScript(script))); //converting directly to Double is not possible. Convert to String, then double.

    }

    //Draws the routes.
    void prepareParcel(int origin_ID, int destination_ID, int parcelClass) {
        ArrayList<String> geolocations = new ArrayList<>();
        String originQuery = "SELECT latitude, longitude FROM Smartpost WHERE Smartpost_ID = '" + origin_ID + "';";
        String destinationQuery = "SELECT latitude, longitude FROM Smartpost WHERE Smartpost_ID = '" + destination_ID + "';";
        String script = "";

        ResultSet rs = DatabaseConnections.getResultset(originQuery);
        ResultSet rs1 = DatabaseConnections.getResultset(destinationQuery);
        try {
            while (rs.next()) { //First get the geolocation of parcel's origin and add it to the arraylist
                String latitude = String.valueOf(rs.getDouble("latitude"));
                String longitude = String.valueOf(rs.getDouble("longitude"));
                geolocations.add(latitude);
                geolocations.add(longitude);
            }
            while (rs1.next()) { //and then destination
                String latitude = String.valueOf(rs1.getDouble("latitude"));
                String longitude = String.valueOf(rs1.getDouble("longitude"));
                geolocations.add(latitude);
                geolocations.add(longitude);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (rs1 != null) {
                    rs1.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        switch (parcelClass) {
        case 1:
            script = "document.createPath(" + geolocations + ", 'red', " + parcelClass + ")";
            break;
        case 2:
            script = "document.createPath(" + geolocations + ", 'green', " + parcelClass + ")";
            break;
        case 3:
            script = "document.createPath(" + geolocations + ", 'blue', " + parcelClass + ")";
            break;
        }
        webView.getEngine().executeScript(script);
    }
}
