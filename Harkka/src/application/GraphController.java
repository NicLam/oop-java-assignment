package application;
import javafx.fxml.FXML;
import javafx.scene.chart.BarChart;
import com.jfoenix.controls.JFXTextArea;
import javafx.scene.chart.XYChart;

public class GraphController {

    @FXML
    private BarChart<?, ?> graph;

    @FXML
    private JFXTextArea graphText;


    void drawShipped(int shippedParcels) { //Draw a diagram of shipped parcels
        XYChart.Series series1 = new XYChart.Series<>(); //Create new series,
        series1.setName("Lähetetty");
        series1.getData().add(new XYChart.Data("", shippedParcels)); //add data to series
        graph.getData().addAll(series1); //add data to graph
    }


    void drawReceived(int receivedParcels) {
        XYChart.Series series1 = new XYChart.Series<>();
        series1.setName("Saapunut");
        series1.getData().add(new XYChart.Data("", receivedParcels));
        graph.getData().addAll(series1);
    }


    void drawParcelsInWarehouse(int warehouseParcels) {
        XYChart.Series series1 = new XYChart.Series<>();
        series1.setName("Varastossa");
        series1.getData().add(new XYChart.Data("", warehouseParcels));
        graph.getData().addAll(series1);
    }

    //User can't edit the text field
    void graphTextNoneditable() {
        graphText.setEditable(false);
    }

    void setGraphText(String string) {
        graphText.appendText(string);
    }

}
