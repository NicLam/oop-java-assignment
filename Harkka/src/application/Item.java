package application;

import java.sql.ResultSet;
import java.sql.SQLException;

class Item {

    //keep track of item ID's here too, and make it accessible so parcel class can add items to database
    private int Item_ID;
    private String name;
    private String size;
    private double mass;
    private boolean fragile;
    private boolean broken;

    //Constructor that is used by the items 1-4 which are already in the database. Turn them into objects.
    Item(int ID, String itemName, String itemSize, double itemMass, boolean itemFragile) {
        name    = itemName;
        size    = itemSize;
        mass    = itemMass;
        fragile = itemFragile;
        broken  = false;
        Item_ID = ID;

        GUIController guiController = Main.getGuiController(); //Get Guicontroller and use its method to add to log.
        String logEntry = "Luotiin esine ID: " + Item_ID + " " + name + " " + size + " " + mass + "kg.";
        guiController.logEntry(logEntry);
    }


    //This constructor is for items which need to be added to database. They'll get their unique ID from this class.
    Item(String itemName, String itemSize, double itemMass, boolean itemFragile) {
        name    = itemName;
        size    = itemSize;
        mass    = itemMass;
        fragile = itemFragile;
        broken  = false;
        Item_ID = setItem_ID();

        DatabaseConnections.insertItem(Item_ID, name, size, mass, fragile); //Add item to database
        GUIController guiController = Main.getGuiController();
        String logEntry = "Luotiin esine ID: " + Item_ID  + " " + name + " " + size + "cm " + mass + "kg.";
        guiController.logEntry(logEntry);
    }

    //Used to make sure the item's height is within the parcel's limits.
    int getItemHeight() {
        String[] temp = size.split("\\*");
        return Integer.parseInt(temp[0]); //size is string such as 1*2*3, where height would be 1.
    }

    //Getters
    String getItemName() {
        return name;
    }

    int getItem_ID() {
        return Item_ID;
    }

    String getItemSize() {
        return size;
    }

    double getItemMass() {
        return mass;
    }

    boolean getItemFragile() {
        return fragile;
    }


    //Setter
    void setItemBroken() {broken = true;}

    private int setItem_ID() {
        int id = 0;
        String query = "SELECT Count(Item_ID) FROM Item;";
        ResultSet rset = DatabaseConnections.getResultset(query);

        try {
            while (rset.next()) {
                id = rset.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (rset != null) {rset.close();}
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return id + 1;
    }
}
