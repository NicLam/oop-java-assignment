/**
 * Created by Niclas Lamponen 0455946
 * SPC 2017
 *
 * This GUI uses JFoenix material design library https://github.com/jfoenixadmin/JFoenix
 *
 * Modifications were made to index.html, I added a new function "getDistance" which will return the distance without drawing anything.
 * It's same as createPath, just without the drawing
 *
 * Documentation has detailed instructions of each method.
 */

package application;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {
    private static GUIController guiController; //Save the instance of GUIController so ParcelCreatorController can call the getter.
    //ParcelCreatorController needs methods in GUIController to check the distance

    @Override
    public void start(Stage primaryStage) {
        //Set up the GUI
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("GUI.fxml"));
            Parent root = fxmlLoader.load();
            guiController = fxmlLoader.getController(); //Cast to (GUIController) if needed

            Scene scene = new Scene(root);

            primaryStage.setTitle("Not-so-smartPost");
            primaryStage.setScene(scene);
            primaryStage.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    static GUIController getGuiController() {
        return guiController;
    }


    public static void main(String[] args) {
        //Initialize SQL tables for the database
        DatabaseConnections.initializeTables();

        //Parse the xml file
        XMLParser.parseData();

        //Initialize warehouse. Singleton pattern since we only have one.
        Warehouse warehouse = Warehouse.getInstance();

        //UI launch
        launch(args);
    }

    @Override //when program is closed
    public void stop() {
        Warehouse.writeWarehouseReceipt();
    }
}
