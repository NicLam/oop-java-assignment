package application;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

class Parcel {
    private ArrayList<Item> contents;
    private int 	parcelID = 0;
    private double 	parcelDistance;
    private double	parcelMass;
    int origin_ID;
    int destination_ID;
    int parcelClass;

    //Use counter and parcel ID to increment parcel's ID every time.
    Parcel(int origin, int destination, double mass, double distance) {
        parcelMass      = mass;
        origin_ID       = origin;
        destination_ID  = destination;
        contents        = new ArrayList<>();
        parcelDistance  = distance;
        parcelID        = setParcelID();
    }


    void addItem(ArrayList<Item> items) {
        contents.clear(); //clear it just to be sure, this way no duplicates enter if user cancels parcel creation midway
        contents.addAll(items);
        DatabaseConnections.insertParcel_Contains(parcelID, contents); //Insert an entry of Parcel and it's contents to database
    }


    //Method below is used to get the amount (=id) of parcels in warehouse, so we can keep adding parcels to that list
    private int setParcelID() {
        int id = 0;
        String query = "SELECT Count(Parcel_ID) FROM Parcel;";
        ResultSet rset = DatabaseConnections.getResultset(query);

        try {
            while (rset.next()) {
                id = rset.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (rset != null) {rset.close();}
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return id + 1; //In other words, return the last item's ID in database + 1.
    }

    //Getters
    ArrayList<Item> getParcelContents() {
        return contents;
    }
    int getParcelClass() {
        return parcelClass;
    }
    int getParcelID() {return parcelID;}
    double getParcelMass() {return parcelMass;}
    int getOrigin_ID() {return origin_ID;}
    int getDestination_ID() {return destination_ID;}
    double getParcelDistance() {return parcelDistance;}
    String parcelContentsTabSeparated() {
        String stringOfItems = "";
        for (Item item : contents) {
            stringOfItems += "\n\tEsineen ID: " + item.getItem_ID() + " " + item.getItemName() + " " + item.getItemSize() + "cm " + item.getItemMass() + "kg";
        }
        return stringOfItems;
    }
}


//Different classes
class FirstClassParcel extends Parcel {
    FirstClassParcel(int originID, int destinationID, double mass, double distance) {
        super(originID, destinationID, mass, distance);
        parcelClass = 1;
        DatabaseConnections.insertParcel(super.getParcelID(), parcelClass);
    }
}

class SecondClassParcel extends Parcel {
    SecondClassParcel(int originID, int destinationID, double mass, double distance) {
        super(originID, destinationID, mass, distance);
        parcelClass = 2;
        DatabaseConnections.insertParcel(super.getParcelID(), parcelClass);
    }
}

class ThirdClassParcel extends Parcel {
    ThirdClassParcel(int originID, int destinationID, double mass, double distance) {
        super(originID, destinationID, mass, distance);
        parcelClass = 3;
        DatabaseConnections.insertParcel(super.getParcelID(), parcelClass);
    }
}
