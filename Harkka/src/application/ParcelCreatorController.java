package application;

import com.jfoenix.controls.*;
import javafx.stage.Stage;
import javafx.scene.control.ToggleGroup;
import javafx.fxml.FXML;

import java.text.DecimalFormat;
import java.util.ArrayList;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ParcelCreatorController {

    //Measurements were taken from posti.fi, sizes S, L, XL.
    private static final int FIRST_CLASS_HEIGHT     = 11; //centimeters
    private static final int SECOND_CLASS_HEIGHT    = 37;
    private static final int THIRD_CLASS_HEIGHT     = 59;

    private static final int PARCEL_WIDTH           = 36; //All parcels have the same width
    private static final int PARCEL_DEPTH           = 60; //All parcels have the same depth

    private static final int PARCEL_MAX_MASS        = 36; //kilograms
    private static final double PARCEL_MAX_VOLUME   = 127.44; //liters

    private double parcelTotalVolume, parcelTotalMass;
    private ArrayList<Item> tempItemList = new ArrayList<>();

    @FXML
    private JFXButton returnButton;

    @FXML
    private JFXTextField newItemNameField, newItemSizeField, newItemMassField;

    @FXML
    private JFXCheckBox newItemFragile;

    @FXML
    private JFXComboBox selectItemBox, fromCityBox, fromSmartpost, toSmartpost, toCityBox;

    @FXML
    private ToggleGroup luokkaSelection;

    @FXML
    private JFXTextArea parcelContent;

    public void initialize() {
        parcelContent.setEditable(false); //User can't edit the content -box
        initializeItemBox(); //Populate comboboxes with entries
        initializeCityBox();
        luokkaSelection.selectToggle(luokkaSelection.getToggles().get(0)); //Select 1st class for the parcel automatically
    }


    //Populate City selection boxes with cities from database
    private void initializeCityBox() {
        String query = "SELECT DISTINCT City FROM SPData"; //Gets distinct cities from SPData, so each city appears once
        ResultSet rs = DatabaseConnections.getResultset(query); //getResultset will return the ResultSet like object that is generated from running the query

        try {
            while (rs.next()) {
                String city = rs.getString("City"); //Get specific column, "City" in this case
                fromCityBox.getItems().addAll(city); //And add it to the list
                toCityBox.getItems().addAll(city);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {rs.close();} //Close the connection, GC should clean it up but this is here just in case.
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }


    //Populate Item box with data from the database
    public void initializeItemBox() {
        selectItemBox.getItems().clear(); //Clear the old entries before adding new
        String query = "SELECT * FROM Item;";
        ResultSet rs = DatabaseConnections.getResultset(query);

        try {
            while (rs.next()) {
                String name = rs.getString("Name");
                String size = rs.getString("Size");
                String mass = rs.getString("Mass");
                selectItemBox.getItems().addAll(name + " " + size + "cm " + mass + "kg");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {rs.close();}
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }


    //Method below will get the Item user has selected/created, it will create an object of it, and adds it to list of temp list of items
    //It also creates database entry if necessary and adds it to the temporary list of items
    public void itemToParcel() {
        String  n = "", s = "";
        int     id = 0;
        double  m = 0;
        boolean f = false;

        //IF user has selected an item from the drop down box.
        if (selectItemBox.getSelectionModel().getSelectedItem() != null) {
            int selection = selectItemBox.getSelectionModel().getSelectedIndex();
            String query = "SELECT * FROM Item LIMIT 1 OFFSET " + selection + ";"; //SQL query to get data of the specific item. This way we get the needed data with ease.
            ResultSet rset = DatabaseConnections.getResultset(query);

            try {
                while (rset.next()) {
                    id = rset.getInt("Item_ID");
                    n = rset.getString("Name");
                    s = rset.getString("Size");
                    m = Double.parseDouble(rset.getString("Mass"));
                    f = rset.getBoolean("Fragile");
                }
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                try {
                    if (rset != null) {rset.close();}
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

            String[] temp = s.split("\\*"); //Get the items height, width and depth to calculate volume
            int h = Integer.parseInt(temp[0]);
            int w = Integer.parseInt(temp[1]);
            int d = Integer.parseInt(temp[2]);

            double volume = (h * w * d) / 1000;

            if (parcelTotalMass + m <= PARCEL_MAX_MASS && volume + parcelTotalVolume <= PARCEL_MAX_VOLUME) { //Check if it fits parcel size & mass -wise
                Item item = new Item(id, n, s, m, f); //Create item and add it to the list of items that will be added to parcel once user selects Class
                tempItemList.add(item);
                updateParcelContent(s, n, m); //Update the content -box
                selectItemBox.getSelectionModel().clearSelection();
            } else {
                parcelContent.appendText("Esineesi ei mahdu minkään luokan pakettiin.\n");
            }
        }

        //IF user has created their own item
        //Takes the size input and splits it to make sure it's requirements fit those of a parcel
        //If they do, it creates an Item instance, and adds it to the temporary list of items
        if (!newItemNameField.getText().isEmpty() && !newItemMassField.getText().isEmpty() && !newItemSizeField.getText().isEmpty()) {
            n = newItemNameField.getText();
            s = newItemSizeField.getText();

            boolean massIsDouble = true;
            try {
                m = Double.parseDouble(newItemMassField.getText());
            } catch (Exception e) {
                massIsDouble = false;
            }

            f = newItemFragile.isSelected();

            if (s.matches("\\d{1,2}\\*\\d{1,2}\\*\\d{1,2}") && massIsDouble && n.length() <= 128) { //Regex to get the size from the selected item, since it'll always match e.g ' 11*11*11 '
                String[] temp = s.split("\\*");
                int h = Integer.parseInt(temp[0]);
                int w = Integer.parseInt(temp[1]);
                int d = Integer.parseInt(temp[2]);
                double volume = (h * w * d) / 1000;

                //Make sure item fits the largest parcel.
                if ((h <= THIRD_CLASS_HEIGHT && h > 0) && (w <= PARCEL_WIDTH && w > 0) && (d <= PARCEL_DEPTH && d > 0) && parcelTotalMass + m <= PARCEL_MAX_MASS && parcelTotalVolume + volume <= PARCEL_MAX_VOLUME) {
                    Item item = new Item(n, s, m ,f); //Create the item
                    tempItemList.add(item);
                    updateParcelContent(s, n, m);
                    newItemSizeField.clear(); newItemMassField.clear(); newItemNameField.clear(); newItemFragile.setSelected(false); //Clear the fields
                } else {
                    parcelContent.appendText("Esineesi ei mahdu minkään luokan pakettiin.\n");
                }
            } else {
                parcelContent.appendText("Tarkista syöte.\n");
            }
        }
    }


    //This method will only add text to the Parcel content box.
    private void updateParcelContent(String stringToParse, String name, double mass) {
        String[] temp = stringToParse.split("\\*");
        int h = Integer.parseInt(temp[0]);
        int w = Integer.parseInt(temp[1]);
        int d = Integer.parseInt(temp[2]);

        parcelTotalVolume += (h * w * d) / 1000;
        parcelTotalMass += mass;

        DecimalFormat formatStyle = new DecimalFormat("##.00");
        parcelContent.appendText("Lisättiin pakettiin: " + name + " " + stringToParse + "cm "+ mass + "kg. Sisältö yht. " + formatStyle.format(parcelTotalVolume) + "L & " + formatStyle.format(parcelTotalMass) + "kg\n");
    }


    //Populate the combobox of cities with Smartpost addresses of a specific city
    public void updateFromBox() {
        fromSmartpost.getItems().clear();
        String city = fromCityBox.getSelectionModel().getSelectedItem().toString();
        String query = "SELECT DISTINCT Address FROM SPData WHERE City = '" + city + "';";
        ResultSet rs = DatabaseConnections.getResultset(query);

        try {
            while (rs.next()) {
                String address = rs.getString("Address");
                fromSmartpost.getItems().addAll(address);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {rs.close();}
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }


    //Populate the 2nd combobox with Smartpost addresses of a specific city, had to be done in 2 different methods since they're dependent on different values.
    public void updateToBox() {
        toSmartpost.getItems().clear();
        String city = toCityBox.getSelectionModel().getSelectedItem().toString();
        String query = "SELECT DISTINCT Address FROM SPData WHERE City = '" + city + "';";
        ResultSet rs = DatabaseConnections.getResultset(query);

        try {
            while (rs.next()) {
                String address = rs.getString("Address");
                toSmartpost.getItems().addAll(address);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {rs.close();}
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }


    //This will get the Smartpost_IDs user has select for their parcel origin & destination
    public void initializeParcel() {
        int originID = 0, destinationID = 0;

        if (fromSmartpost.getSelectionModel().getSelectedItem() != null && toSmartpost.getSelectionModel().getSelectedItem() != null) {
            String origin      = fromSmartpost.getSelectionModel().getSelectedItem().toString();
            String destination = toSmartpost.getSelectionModel().getSelectedItem().toString();
            ArrayList<SmartPostInstances> SPList = SmartPostInstances.getSPList();

            for (SmartPostInstances sp : SPList) {
                if (sp.SPaddress.equals(origin)) {
                    originID = sp.SPId;
                }
                if (sp.SPaddress.equals(destination)) {
                    destinationID = sp.SPId;
                }
            }
        }

        //If user hasn't selected class, added items or selected parcel destination/origin, it'll notify them about it
        if (luokkaSelection.getSelectedToggle() == null || tempItemList.isEmpty() || originID == 0 || destinationID == 0) {
            parcelContent.appendText("Et ole valinnut paketin luokkaa, lähtökaupunkia, kohdetta tai pakettisi on tyhjä.\n");

            //Else if user has given needed info and all items fit, it will create the parcel in "checkItemsAndCreateParcel"
        } else if (checkItemsAndCreateParcel(tempItemList, originID, destinationID)) {
            closeWindow(); //Close the window once parcel has been created
        } else {
            parcelContent.appendText("Tavarasi eivät mahdu nykyisen luokan kuljetukseen tai yrität lähettää 1lk pakettia liian kauas!\n");
        }
    }


    /**Maybe slightly dirty, but works. It checks which class user has select for the parcel. Then runs through the list of items 'tempItemList'
     * Checks if every item fits the parcel's size requirements, and if they do, it "adds them into the parcel" (Parcel class keeps list of them)
     * If it doesn't, it'll notify the user to select another class
     * Eventually adds the parcel in to the warehouse if everything fits and is OK
     */

    private boolean checkItemsAndCreateParcel(ArrayList<Item> tempItemList, int originID, int destinationID) {
        boolean itemsFit = true;
        JFXToggleButton selectedClass = (JFXToggleButton)luokkaSelection.getSelectedToggle();

        GUIController guiController = Main.getGuiController(); //Get the GUIController object, so we can use its method "createPath" and get the distance parcel has to travel.
        Double distance = guiController.getDistance(originID, destinationID); //Check the distance of 1st class parcel here before creation already

        switch (selectedClass.getText()) {
        case "1. Luokka":

            if (distance > 150) {
                return false;
            }

            for (Item item : tempItemList) { //Loop through the temporary list of items to make sure they all fit the parcel's class
                if (item.getItemHeight() > FIRST_CLASS_HEIGHT) {
                    itemsFit = false;
                }
            }

            if (itemsFit) { //If all is good, it is going to create the Parcel object and add the items from templist to the parcel
                FirstClassParcel firstClassParcel = new FirstClassParcel(originID, destinationID, parcelTotalMass, distance);
                firstClassParcel.addItem(tempItemList);
                Warehouse.addParcel(firstClassParcel); //And add the parcel to warehouse
            }
            break;

        case "2. Luokka":
            for (Item item : tempItemList) {
                if (item.getItemHeight() > SECOND_CLASS_HEIGHT) {
                    itemsFit = false;
                }
            }
            if (itemsFit) {
                SecondClassParcel secondClassParcel = new SecondClassParcel(originID, destinationID, parcelTotalMass, distance);
                secondClassParcel.addItem(tempItemList);
                Warehouse.addParcel(secondClassParcel);
            }
            break;

        case "3. Luokka":
            ThirdClassParcel thirdClassParcel = new ThirdClassParcel(originID, destinationID, parcelTotalMass, distance);

            //No checks are needed since they are already done on the user input. All items always fit third class.

            thirdClassParcel.addItem(tempItemList);
            Warehouse.addParcel(thirdClassParcel);
            break; //Redundant for now. If we'll expand, maybe it'll be useful.
        }
        return itemsFit;
    }


    //Clear users Item -selection
    public void clearItemSelection() {
        selectItemBox.getSelectionModel().clearSelection();
    }


    //Close the window
    public void closeWindow() {
        Stage stage = (Stage) returnButton.getScene().getWindow();
        stage.close();
    }
}
