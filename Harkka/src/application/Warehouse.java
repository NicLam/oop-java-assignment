package application;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.io.File;

public class Warehouse {
    private static Warehouse instance;
    private static ArrayList<Parcel> parcelsInWarehouse = new ArrayList<>();
    private static int shippedParcels = 0, receivedParcels = 0;
    private static final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy HH.mm.ss");

    private Warehouse() {

    }

    //This delivery network utilizes only one warehouse. Singleton. Also empties the file which has log.
    static Warehouse getInstance() {
        if (instance == null) {
            instance = new Warehouse();

            File file = new File("receipt.txt");
            try {
                if (file.createNewFile()) {
                    System.out.println("Receipt was created in the IDE working directory");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return instance;
    }


    static void addParcel(Parcel parcel) {
        receivedParcels++;
        parcelsInWarehouse.add(parcel);
        String logText = "Luotiin paketti ja siirrettiin varastoon. Paketin ID: " + parcel.getParcelID() + " " + parcel.getParcelMass() +
            "kg. Paketteja jäljellä varastossa " + parcelsInWarehouse.size() + "kpl.";
        GUIController guiController = Main.getGuiController();
        guiController.logEntry(logText);
    }


    static void shipParcel(Parcel parcel) {
        shippedParcels++;
        GUIController guiController = Main.getGuiController();

        if (parcelsInWarehouse.contains(parcel)) {
            parcelsInWarehouse.remove(parcel);
            String logText = "Lähetettiin " + parcel.getParcelClass() + "lk paketti. ID: " + parcel.getParcelID() + " " + parcel.getParcelMass() + "kg "
                + parcel.getParcelDistance() + "km päähän. Sisältö:" + parcel.parcelContentsTabSeparated() + "\nPaketteja jäljellä varastossa " + parcelsInWarehouse.size() + "kpl.";
            guiController.deliveryEntry(parcel, logText);
        }

        guiController.prepareParcel(parcel.origin_ID, parcel.destination_ID, parcel.getParcelClass());

        if (parcel.getParcelClass() == 1 || parcel.getParcelClass() == 3) { //Fragile items in 1st & 3rd class parcels will break
            ArrayList<Item> items = parcel.getParcelContents();
            for (Item item : items) {
                if (item.getItemFragile()) {
                    item.setItemBroken();
                    guiController.logEntry("Lähetyksen aikana paketissa ID " + parcel.getParcelID() + " hajosi esine " + item.getItemName() + ", ehkä ensi kerralla kannattaisi tarkistaa paketien luokat.");
                }
            }
        }
    }


    static void writeWarehouseReceipt() {
        int id = 0; //Receipt ID to not to override previous receipts //TODO is this even needed
        String query = "SELECT Count(Receipt_ID) FROM Receipt;";
        ResultSet rset = DatabaseConnections.getResultset(query);

        try {
            while (rset.next()) {
                id = rset.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (rset != null) {rset.close();}
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        id += 1;

        BufferedWriter bw = null;
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        try {
            DatabaseConnections.insertReceipt(id, shippedParcels, receivedParcels, parcelsInWarehouse.size());
            bw = new BufferedWriter(new FileWriter("receipt.txt"));
            bw.write(simpleDateFormat.format(timestamp) + " - Paketteja saapui: " + receivedParcels + ".\nPaketteja lähetettiin: " + shippedParcels + "\nPaketteja jäi lähettämättä: " + parcelsInWarehouse.size());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (bw != null) {bw.close();}
            } catch (IOException e) {
                System.out.println("Virhe suljettaessa BufferedReaderia.\n");
            }
        }
    }


    //Getters
    public static int getAmountOfParcels() {
        return parcelsInWarehouse.size();
    }

    public static int getShippedParcels() {
        return shippedParcels;
    }

    public static int getReceivedParcels() {
        return receivedParcels;
    }

    public static ArrayList<Parcel> getListOfParcels() {
        return parcelsInWarehouse;
    }
}
