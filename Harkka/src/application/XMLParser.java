package application;

import java.net.URL;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class XMLParser {
    public XMLParser() {

    }


    public static void parseData() {
        int smartpost_ID = 1;

        try {
            System.out.println("Parsing XML from the URL and inserting data into database...");

            //DocumentBuilder to create the needed document from the XML.
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document doc = db.parse(new URL("http://smartpost.ee/fi_apt.xml").openStream());
            doc.getDocumentElement().normalize();

            // NodeList of elements with parent tag "place" in doc
            NodeList nodes = doc.getElementsByTagName("place");

            // Parsing and formatting, getting correct subtags from the xml doc

            for (int i = 0; i < nodes.getLength(); i++) { //Run through the list, change the current object's type to Element so parsing it will be possible.
                Node current = nodes.item(i);
                Element eElement = (Element) current;

                String code         = (eElement.getElementsByTagName("code").item(0).getTextContent());
                String city         = (eElement.getElementsByTagName("city").item(0).getTextContent());
                String address      = (eElement.getElementsByTagName("address").item(0).getTextContent());
                String availability = (eElement.getElementsByTagName("availability").item(0).getTextContent());
                String postoffice   = (eElement.getElementsByTagName("postoffice").item(0).getTextContent());
                double lat          = Double.parseDouble((eElement.getElementsByTagName("lat").item(0).getTextContent()));
                double lng          = Double.parseDouble((eElement.getElementsByTagName("lng").item(0).getTextContent()));

                // Create new instances, SPI Class keeps list of them all in ArrayList
                SmartPostInstances smartPostAutomat = new SmartPostInstances(smartpost_ID, code, city, address, availability,
                                                                             postoffice, lat, lng);

                //Feed them into the database tables "Smartpost" and "Postal_Code_City"
                DatabaseConnections.insertSmartpost(smartpost_ID, code, address, availability, lat, lng);
                DatabaseConnections.insertPostalCodeCity(code, city);
                smartpost_ID++;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            System.out.println("Done!");
        }
    }
}
